package dayTwo;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class Calculator {

	public static void main (String[] args){
		Scanner reader = new Scanner(System.in);
		
		do {
			System.out.println("Gimme a number: ");
			int n1 = reader.nextInt();
			System.out.println("Gimme an operation(+,-,*, or /): ");
			String operation = reader.next();
			System.out.println("Gimme another number: ");
			int n2 = reader.nextInt();
			double answer = 0;
			if(operation.equals("+")) {
				answer = n1+n2;
			} else if(operation.equals("-")) {
				answer = n1-n2;
			} else if(operation.equals("*")) {
				answer = n1*n2;
			} else if(operation.equals("/")) {
				answer = (double)n1/n2;
			} else {
				System.out.println("That wasn't one of the operaions I asked for!");
				continue;
			}
			System.out.println("Your answer: " + answer);
			
		}while(JOptionPane.showConfirmDialog(null, "Try another operation?") == 0);
		
		reader.close();
	}
}
