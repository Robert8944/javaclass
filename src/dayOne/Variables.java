package dayOne;

public class Variables {

	public static void main(String[] args) {
		int age = 3;
		char firstInitial = 'R';
		char secondInitial = 'A';
		char thirdInitial = 'M';
		char[] initials = {firstInitial, secondInitial, thirdInitial};
		final String NAME = new String("Robert");
		String nickName = "Bob";
		System.out.println("Hello I am "+age+" years old my name is " + NAME + ", but I go by " + nickName);
		System.out.println("My initials are " + initials[0] + "." + initials[1] + "." + initials[2] + ".");
		/*one can also complete the above statements in the following way:
		 * System.out.println("Hello I am "+age+" years old my name is " + NAME + ", but I go by " + nickName + "\nMy initials are " + initials[0] + "." + initials[1] + "." + initials[2] + ".");
		 */
	}

}
